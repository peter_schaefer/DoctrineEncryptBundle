<?php

namespace Ambta\DoctrineEncryptBundle\Tests\Unit\Encryptors;

use Ambta\DoctrineEncryptBundle\Encryptors\CipherSweetEncryptor;
use Ambta\DoctrineEncryptBundle\Encryptors\HaliteEncryptor;
use PHPUnit\Framework\TestCase;

class CipherSweetEncryptorTest extends TestCase
{
    private const DATA = 'foobar';

    public function testEncryptExtension(): void
    {
        $keyfile = __DIR__.'/fixtures/ciphersweet.key';
        $key = file_get_contents($keyfile);
        $halite = new CipherSweetEncryptor($key);

        $encrypted = $halite->encrypt(self::DATA);
        $this->assertNotSame(self::DATA, $encrypted);
        $decrypted = $halite->decrypt($encrypted);

        $this->assertSame(self::DATA, $decrypted);
        $newkey = file_get_contents($keyfile);
        $this->assertSame($key, $newkey, 'The key must not be modified');
    }

    public function testEncryptExtensionSupportsLegacyKeys(): void
    {
        $keyfile = __DIR__.'/fixtures/ciphersweet.legacy.key';
        $key = file_get_contents($keyfile);
        $halite = new CipherSweetEncryptor($key);

        $encrypted = $halite->encrypt(self::DATA);
        $this->assertNotSame(self::DATA, $encrypted);
        $decrypted = $halite->decrypt($encrypted);

        $this->assertSame(self::DATA, $decrypted);
        $newkey = file_get_contents($keyfile);
        $this->assertSame($key, $newkey, 'The key must not be modified');
    }

    public function testEncryptWithoutExtensionThrowsException(): void
    {
        if (extension_loaded('sodium')) {
            $this->markTestSkipped('This only runs when the sodium extension is disabled.');
        }
        $keyfile = __DIR__.'/fixtures/halite.key';
        $halite = new HaliteEncryptor($keyfile);

        $this->expectException(\SodiumException::class);
        $halite->encrypt(self::DATA);
    }
}
