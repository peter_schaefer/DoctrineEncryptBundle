<?php

namespace Ambta\DoctrineEncryptBundle\Encryptors;

use Defuse\Crypto\Crypto;

/**
 * Class for encrypting and decrypting with the defuse library
 *
 * @author Michael de Groot <specamps@gmail.com>
 */
class DefuseEncryptor implements EncryptorInterface
{
    private string $key;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * {@inheritdoc}
     */
    public function encrypt($data)
    {
        return Crypto::encryptWithPassword($data, $this->key);
    }

    /**
     * {@inheritdoc}
     */
    public function decrypt($data)
    {
        return Crypto::decryptWithPassword($data, $this->key);
    }
}
