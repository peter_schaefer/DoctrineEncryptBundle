<?php

namespace Ambta\DoctrineEncryptBundle\Encryptors;

/**
 * Indexer interface for encryptors
 */
interface IndexerInterface
{
    /**
     * @param string $data Plain text to encrypt
     * @return string Encrypted text
     */
    public function getIndexValue($data);
}
