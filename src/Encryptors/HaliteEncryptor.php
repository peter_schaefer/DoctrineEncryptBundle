<?php

namespace Ambta\DoctrineEncryptBundle\Encryptors;

use ParagonIE\ConstantTime\Hex;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\Symmetric\Crypto;
use ParagonIE\Halite\Symmetric\EncryptionKey;
use ParagonIE\HiddenString\HiddenString;

/**
 * Class for encrypting and decrypting with the halite library
 *
 * @author Michael de Groot <specamps@gmail.com>
 */
class HaliteEncryptor implements EncryptorInterface
{
    private $encryptionKey;

    public function __construct(string $key)
    {
        $this->encryptionKey = new EncryptionKey(
            new HiddenString(
                KeyFactory::getKeyDataFromString(Hex::decode($key))
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function encrypt($data)
    {
        return Crypto::encrypt(new HiddenString($data), $this->encryptionKey);
    }

    /**
     * {@inheritdoc}
     */
    public function decrypt($data)
    {
        $data = Crypto::decrypt($data, $this->encryptionKey);
        if ($data instanceof HiddenString) {
            $data = $data->getString();
        }

        return $data;
    }
}
