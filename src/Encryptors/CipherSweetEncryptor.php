<?php

namespace Ambta\DoctrineEncryptBundle\Encryptors;

use ParagonIE\CipherSweet\Backend\FIPSCrypto;
use ParagonIE\CipherSweet\BlindIndex;
use ParagonIE\CipherSweet\CipherSweet;
use ParagonIE\CipherSweet\EncryptedField;
use ParagonIE\CipherSweet\KeyProvider\StringProvider;
use ParagonIE\ConstantTime\Hex;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\Symmetric\EncryptionKey;
use ParagonIE\HiddenString\HiddenString;
use Throwable;

/**
 * Class for encrypting and decrypting with the CipherSweet library
 */
class CipherSweetEncryptor implements EncryptorInterface, IndexerInterface
{
    private EncryptedField $field;

    public function __construct(string $key)
    {
        try {
            $encKey = new StringProvider($key);
        } catch (Throwable $t) {
            $encKey = new EncryptionKey(new HiddenString(KeyFactory::getKeyDataFromString(Hex::decode($key))));
            $encKey = new StringProvider($encKey->getRawKeyMaterial());
        }
        $engine = new CipherSweet($encKey, new FIPSCrypto());
        $this->field = (new EncryptedField($engine, 'entity', 'field'))
            ->addBlindIndex(new BlindIndex('blind_idx', [], 32, true));
    }

    /**
     * {@inheritdoc}
     */
    public function encrypt($data)
    {
        return $this->field->encryptValue($data);
    }

    /**
     * {@inheritdoc}
     */
    public function decrypt($data)
    {
        return $this->field->decryptValue($data);
    }

    public function getIndexValue($data): string
    {
        return $this->field->getBlindIndex(strtolower(trim($data)), 'blind_idx');
    }
}
